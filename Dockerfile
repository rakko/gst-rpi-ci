FROM rakko/crossbuild-gst-rpi

# Need ssh and libnss_wrapper to upload to images.ccu
RUN sudo apt-get update && sudo apt-get install -y ssh libnss-wrapper
