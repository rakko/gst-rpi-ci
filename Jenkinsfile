pipeline {
  options { timestamps() }

  environment {
    GIT_AUTHOR_NAME  = "Collabora Jenkins"
    GIT_AUTHOR_EMAIL = "info@collabora.co.uk"
    OUTPUT_DIR = "${env.WORKSPACE}/output"
    ARTIFACT = "gstreamer-1.0-linux-armv7-build-${env.BUILD_ID}.tar.bz2"
  }

  agent {
    dockerfile {
      additionalBuildArgs '--pull'
    }
  }

  stages {
    stage("Setup") {
      steps {
        sh "mkdir -p ${OUTPUT_DIR}"
        sh "cd /gstbuild && ./setup.sh"
      }
    }

    stage("Cleanup") {
        steps {
            sh(script:"cd ${WORKSPACE}; rm -rf *.tar.bz2 **/*.tar.bz2")
        }
    }

    stage("cerbero bootstrap") {
      steps {
        sh "/gstbuild/cerbero/cerbero-uninstalled -c /gstbuild/cerbero/config/cross-lin-rpi.cbc bootstrap --build-tools-only"
      }
    }

    stage("cerbero package") {
      steps {
        sh "/gstbuild/cerbero/cerbero-uninstalled -c /gstbuild/cerbero/config/cross-lin-rpi.cbc package -t gstreamer-1.0 --no-split -o ${OUTPUT_DIR}"
      }
    }

    stage("Upload") {
      environment {
          NSS_WRAPPER_PASSWD = '/tmp/passwd'
          NSS_WRAPPER_GROUP = '/dev/null'
          ARTIFACT_DIR = '/srv/jenkins-upload/www/images/gstreamer-rpi/test'
      }
      steps {
        sh(script:'mv "${OUTPUT_DIR}"/*.tar.bz2 "${OUTPUT_DIR}/${ARTIFACT}"')
        script {
          sshagent (credentials: [ "images.ccu-upload", ] ) {
            sh 'echo docker:x:$(id -u):$(id -g):docker gecos:/tmp:/bin/false > ${NSS_WRAPPER_PASSWD}'
            sh 'LD_PRELOAD=libnss_wrapper.so scp -oStrictHostKeyChecking=no ${OUTPUT_DIR}/${ARTIFACT} jenkins-upload@images.collabora.co.uk:${ARTIFACT_DIR}'
            sh 'LD_PRELOAD=libnss_wrapper.so ssh -oStrictHostKeyChecking=no jenkins-upload@images.collabora.co.uk "cd ${ARTIFACT_DIR}; ln -sf ${ARTIFACT} latest-build"'
          }
        }
      }
    }
  }
}
